package j.binarytree;

import java.util.ArrayList;

public class BinaryTree {

    private final BinaryTreeNode root;

    /**
     * @param object the object to store in the (root) Node
     * @param value the value for sorting
     */
    public BinaryTree(Object object, double value) {
        root = new BinaryTreeNode(object, value);
    }

    public void add(Object obj, double val) {
        root.add(obj, val);
    }

    public void add(ArrayList<Object> list, double val) {
        for (Object obj : list) {
            root.add(obj, val);
        }
    }

    public Object search(double val) {
        return root.search(val);
    }

    public ArrayList<Object> getSortedList() {
        return root.getSortedList(new ArrayList<>());
    }
}
