package j.binarytree;

import java.util.ArrayList;

public class BinaryTreeNode {

    private BinaryTreeNode left;
    private BinaryTreeNode right;
    private Object object;
    private double value;

    BinaryTreeNode(Object object, double value) {
        this.object = object;
        this.value = value;
        left = right = null;
    }

    public void add(Object obj, double val) {
        if (val == this.value) {
            //System.err.println("value " + val + " is already used, ignoring...");
        } else if (val < this.value) {
            if (left == null) {
                left = new BinaryTreeNode(obj, val);
            } else {
                left.add(obj, val);
            }
        } else if (val > this.value) {
            if (right == null) {
                right = new BinaryTreeNode(obj, val);
            } else {
                right.add(obj, val);
            }
        }
    }

    public ArrayList<Object> getSortedList(ArrayList<Object> list) {
        if (left != null) {
            left.getSortedList(list);
        }
        list.add(object);
        if (right != null) {
            right.getSortedList(list);
        }

        return list;
    }

    public Object search(double val) {
        if (val == this.value) {
            return this.object;
        }
        if (left != null && val < this.value) {
            return left.search(val);
        }
        if (right != null && val > this.value) {
            return right.search(val);
        }
        return null;

    }
}
